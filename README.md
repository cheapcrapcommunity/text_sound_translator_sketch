# text_sound_translator_sketch



## Documentation

This is a text-sound translator. The user input is handwritten text, and the writing data is mapped to frequency/MIDI notes. The large blank square box in the middle of the screen is the writing area, and each word will be converted and stored in the grey rectangle. When the user finishes writing a word or a sentence, click the speaker button below to start generating sound. It could be a sound for a single word or a whole sentence. The menu on the left of screen offers options to adjust the size of pen and clear the canvas, the menu on the right shows the forms of sound, e.g., the whole spectrum of frequency mapping or MIDI notes mapping. These menus are extensible if there’re more functions need to be added in the future.

## Basic Functions

The function from each part from left to right and up and down as follows:

•	User editing menu

•	Handwriting area

•	Output sonification options

•	Converted text area

•	Speaker button 


The button turns to grey if the mouse/pen is inside the button. After the user clicks it, the whole interface turns into grey color and starts to generate and play sound. 
