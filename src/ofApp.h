#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofPolyline polyline;
		ofFbo fbo;
		int width = ofGetHeight() - ofGetHeight() / 3;
		int height = ofGetHeight()-ofGetHeight() / 3;

		ofxPanel leftGui;
		ofxIntSlider size;
		ofxButton btnClear;
		
		ofxPanel rightGui;
		ofxToggle frequency;
		ofxToggle midi;

		ofImage image;
		bool gensound;
		string wait = "generating sound...... ";
		string text = "convert handwriting to text";
};
