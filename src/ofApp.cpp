#include "ofApp.h"

#include <functional>


class RectButtonListener {

public:
	// set up callback function type for mouse clicked x,y events
	typedef std::function<bool(int, int)> mouseClickCallbackFunction;

	// constructor -- MUST have all properties set at the start!
	RectButtonListener(ofRectangle& initHitArea, const mouseClickCallbackFunction callbackFunction)
		:
		hitArea(initHitArea), _callbackFunction(callbackFunction), draw(NULL)
	{}

	// easy way to run the callback function
	bool operator()(int x, int y) {
		bool result = false;

		// run callback if mouse is inside the rectangle
		if (this->hitArea.inside(x, y))
		{
			result = this->_callbackFunction(x, y);
		}
		return result;
	}

	mouseClickCallbackFunction draw;

	ofRectangle hitArea; // area of button on screen

private:
	mouseClickCallbackFunction _callbackFunction; // function to run
};
// end rectangle button class

// a list of rectangular areas to click
std::vector<RectButtonListener> rectButtons;

//--------------------------------------------------------------
void ofApp::setup() {

	ofBackground(255);

	fbo.allocate(width, height);
	fbo.begin();
	ofClear(255);
	fbo.end();

	leftGui.setup();
	leftGui.add(size.setup("size", 3, 1, 8));
	leftGui.add(btnClear.setup("clear"));

	rightGui.setup();
	rightGui.setPosition(ofGetWidth()-210, 10);
	rightGui.add(frequency.setup("frequency", 1, 3, 20));
	rightGui.add(midi.setup("MIDI", 0, 3, 20));

	ofRectangle buttonArea(
		ofGetWidth() / 2 - ofGetWidth() / 20, height+ofGetHeight()/7,
		ofGetWidth() / 10, ofGetWidth() / 10
	);

	image.load("speaker.png");

	// add a 'button' quickly
	rectButtons.push_back(
		RectButtonListener(
			buttonArea,
			[](int mx, int my) {
		//fadeTicks = 200;
		return true;
	}
		)
	);

	buttonArea.setPosition(ofGetWidth() / 10, ofGetHeight() / 10);

	glm::vec3 ctr = buttonArea.getCenter();
	
	gensound = false;
}

//--------------------------------------------------------------
void ofApp::update() {

	if (btnClear) {
		fbo.begin();
		ofClear(255);
		fbo.end();
	}
}

//--------------------------------------------------------------
void ofApp::draw() {

	//draw the left gui for user to change size of pen and clear the current writting
	leftGui.draw();
	//draw the right gui for user to choose which type of sound they want to convert the text to
	rightGui.draw();

	//draw a container for handwritting area
	ofFill();
	ofSetColor(0);
	fbo.draw(ofGetWidth() / 2 - width / 2, 0);
	//draw the border of the handwritting area
	ofNoFill();
	ofDrawRectangle(ofGetWidth() / 2 - width / 2, 0, width, height);

	//draw a rectangle representing the text(ocr) area
	ofFill();
	ofSetColor(0,100);
	ofDrawBitmapString(text, 5, height +10+ height / 12);
	ofDrawRectangle(0, height+10, ofGetWidth(), height / 6);
	ofNoFill();
	//draw a grey screen with words on it after clicking button
	if (gensound) {
		ofFill();
		ofSetColor(0);
		ofDrawBitmapString(wait, ofGetWidth() / 2 - 80, ofGetHeight() / 2);	
		ofFill();
		ofSetColor(0, 100);
		ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
	}
	ofNoFill();
	
	// draw button:

	image.draw(ofGetWidth() / 2 - ofGetWidth() / 20, height + ofGetHeight() / 7,
		ofGetWidth() / 10, ofGetWidth() / 10);

	for (std::vector<RectButtonListener>::const_iterator rectButton = rectButtons.begin(); rectButton != rectButtons.end(); ++rectButton) {
		if ((*rectButton).draw != NULL) {
			(*rectButton).draw(mouseX, mouseY);
		}

		else {
			//default draw
			if ((*rectButton).hitArea.inside(mouseX, mouseY)) {
				ofFill(); ofSetColor(0, 100);
			}
			else {
				ofNoFill();
			}
			ofDrawRectangle((*rectButton).hitArea);

		}
	}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	fbo.begin();
	ofSetLineWidth(size);
	ofSetColor(0);
	polyline.addVertex(ofPoint(x-width/2, y));
	polyline.draw();
	fbo.end();
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{

	fbo.begin();
	polyline.clear();
	fbo.end();

	// go through all callbacks and run them -- if they return true, mouse was inside

	for (std::vector<RectButtonListener>::iterator rectButton = rectButtons.begin(); rectButton != rectButtons.end(); ++rectButton) {
		bool result = (*rectButton)(mouseX, mouseY);
		//status changed after clicking button
		if (result) {
			//cout << "hit one! " << endl;
			if (!gensound) {
				gensound = true;
			}
		}		
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {
	if (gensound) {
		gensound = false;
	}
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
